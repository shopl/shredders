MenuInit();

function MenuInit() {

  var state = false;

  var $menu = document.querySelector('.megamenu');
  var $body = document.querySelector('body');
  var $menuOverlay = document.querySelector('[data-megamenu-bg]');

  document.querySelector('.js-menu-open').addEventListener('click', function(e) {
    e.preventDefault();

    if(state == false) {
      state = true;
      $menu.classList.add('open');
      $menuOverlay.style.display = 'block';
      setTimeout(function() {
        $body.classList.add('menu-open');
      })
    }
    else {
      state = false;
      $menu.classList.remove('open');
      setTimeout(function() {
        $body.classList.remove('menu-open');
        setTimeout(function() {
          $menuOverlay.style.display = 'none';
        }, 300);
      }, 300);
    }

  })
}